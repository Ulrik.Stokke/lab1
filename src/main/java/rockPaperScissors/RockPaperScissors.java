package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    private String random_choice() {
        int n;
        n = (int) ((Math.random() * (3-0)) + 0);
        return rpsChoices.get(n);
    }

    private boolean is_winner(String choice1,String choice2) {
        if (choice1.equals("paper"))
            return choice2.equals("rock");
        else if (choice1.equals("scissors"))
            return choice2.equals("paper");
        else
            return (choice2.equals("scissors"));
    }

     private boolean validate_input(String input, List valid_inputs) {
         input = input.toLowerCase();
         return valid_inputs.contains(input);
     }

    private String user_choice(){
        String input = readInput("Your choice (Rock/Paper/Scissors)?");
        String choice1 = input.toLowerCase();
        if (validate_input(choice1, rpsChoices))
            return choice1;
        else
            System.out.println("I don't understand" + input + ". Try again");
        return input;

    }

    public String continue_playing(){
        String input = readInput("Do you wish to continue playing? (y/n)?");
        String continue_answer = input.toLowerCase();
        if (validate_input(continue_answer, Arrays.asList("y","n")))
            return continue_answer;
        else
            System.out.println("I don't understand" + input + ". Try again");
        return input;

    }

     public void run() {

         while (true) {
             System.out.println("Let's play round " + roundCounter);
             roundCounter += 1;
             String human_choice = user_choice();
             String com_choice = random_choice();
             if (is_winner(human_choice, com_choice)) {
                 System.out.println("Human chose " + human_choice + " computer chose " + com_choice + ". Human wins!");
                 humanScore += 1;
             }
             else if (is_winner(com_choice,human_choice)) {
                 System.out.println("Human chose " + human_choice + " computer chose " + com_choice + ". Computer wins!");
                 computerScore += 1;
             }
             else
             System.out.println("Human chose " + human_choice + " computer chose " + com_choice + ". It's a tie!");
             System.out.println("Score: human " + humanScore + " computer " + computerScore);
             if (continue_playing().equals("n"))
                 break;
         }
         System.out.println("Bye bye :)");
    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
